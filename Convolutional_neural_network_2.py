
# Author : Marion Estoup
# E-mail : marion_110@hotmail.fr
# February 2023

###################################### Convolutional neural network CNN example


############## Import  libraries
import tensorflow as tf
from tensorflow import keras
from sklearn.metrics import confusion_matrix
import seaborn as sn
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import sys,os
from importlib import reload
import math



################################################## First step

# Vector of image and vector of data 
(x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()
x_train = x_train.reshape(-1,28,28,1)
x_test = x_test.reshape(-1,28,28,1)
# We have 60 000 pictures in x_train and 10 000 pictures in x_test
# x_train = each picture and y_train = each class
# reshape to flatten pictures


print("x_train : ",x_train.shape)
print("y_train : ",y_train.shape)
print("x_test  : ",x_test.shape)
print("y_test  : ",y_test.shape)

# Show the 27th picture of x_train
plt.imshow(x_train[27], cmap = 'binary')
plt.show()





################################################## Second step
print('Before normalization : Min={}, max={}'.format(x_train.min(),x_train.max()))

# Data normalization
xmax = x_train.max()
x_train = x_train / xmax
x_test = x_test / xmax 

print('After normalization  : Min={}, max={}'.format(x_train.min(),x_train.max()))




################################################# Third step
plt.imshow(x_train[27], cmap = 'binary')
plt.show()






################################################ Fourth step

# Vizualize the different classes and numbers associated to the classes
rows = 5
cols = 5
axes=[]
fig=plt.figure()
t=0
for a in range(rows*cols):
	axes.append( fig.add_subplot(rows, cols, a+1) )
	subplot_title=("class "+str(y_train[t]))
	axes[-1].set_title(subplot_title, fontsize=6)
	axes[-1].axes.set_axis_off()
	plt.imshow(x_train[t], cmap = 'binary')
	t+=1
fig.tight_layout()    
plt.show()







################################################ Fifth step


# Sequential model (first layer, second layer etc)
model = keras.models.Sequential() 
# We add a first layer in our model with a specific dimension 28*28*1 (1D here, not 2D)
model.add(keras.layers.Input((28,28,1)))
# First layer is the input layer


# We add a second layer which is a convolution layer but we could add any type of layer
model.add(keras.layers.Conv2D(8, (3,3), activation='relu'))
# We use 8 convolution kernels of size 3*3
# activation function is relu 


# Then we do a max pooling of 2*2 so we reduce x and y by a factor of 2
model.add(keras.layers.MaxPooling2D((2,2)))

# All layers are connected which can lead to overfitting
# That's why we turn off a specific percentage of those connections
# Here we put a dropout of 20%
model.add(keras.layers.Dropout(0.2))


# We do the same thing again (adding layers, pooling and dropout)
model.add(keras.layers.Conv2D(16, (3,3), activation='relu'))
model.add(keras.layers.MaxPooling2D((2,2)))
model.add(keras.layers.Dropout(0.2))


# In the end we have to flat everything again (all our pictures and we combine them into one vector)
model.add(keras.layers.Flatten())

# Then, a basic network with a dense layer of 100
model.add(keras.layers.Dense(100, activation='relu'))
model.add(keras.layers.Dropout(0.5)) # and dropdout of 50%

# Output with softmax to get to know the best output (10 is for the 10 class)
model.add(keras.layers.Dense(10, activation='softmax'))

# That's it, we created a sequential network !


# We can have a look at our model
model.summary()

# Now we can train our model
# We compile the network
model.compile(optimizer='adam',
              loss='sparse_categorical_crossentropy', # loss function often used for pictures
              metrics=['accuracy']) 










################################################### Sixth step

# We define a batch size and a number of epochs
batch_size = 512 
epochs=16 

# We run the training for our network 
history = model.fit(x_train,y_train,
                    batch_size = batch_size,
                    epochs= epochs,
                    verbose=1,
                    validation_data = (x_test, y_test)) # validation dataset
                    

# Model evaluation
score = model.evaluate(x_test,y_test,verbose=0)
print(f'Test loss : {score[0]:4.4f}') # 0.0423
print(f'Test accuracy : {score[1]:4.4f}') # 0.9867








################################################# Seventh step

# Plot loss function and accuracy values over the epochs
def plot_history(history, figsize=(8,6), plot={"Accuracy":['accuracy','val_accuracy'], 'Loss':['loss', 'val_loss']}):
	fig_id=0
	for title,curves in plot.items():
		plt.figure(figsize=figsize)
		plt.title(title)
		plt.ylabel(title)
		plt.xlabel('Epoch')
		for c in curves:
			plt.plot(history.history[c])
		plt.legend(curves, loc='upper left')
		plt.show()
plot_history(history)


# Define y_sigmoid and y_pred
y_sigmoid = model.predict(x_test)
y_pred    = np.argmax(y_sigmoid, axis=-1)

# Define errors
errors=[ i for i in range(len(x_test)) if y_pred[i]!=y_test[i] ]
errors=errors[:min(24,len(errors))]

# Look at all the predicted classes with the corresponding pictures
rows = 10
cols = 5
axes=[]
fig=plt.figure()
t=0
for a in range(rows*cols):
	axes.append( fig.add_subplot(rows, cols, a+1) )
	subplot_title=("class "+str(y_test[t])+" --> "+str(y_pred[t]))
	if y_test[t]!=y_pred[t]:
		axes[-1].set_title(subplot_title, color= 'red',fontsize=6)
	else:
		axes[-1].set_title(subplot_title, fontsize=6)
	axes[-1].axes.set_axis_off()
	plt.imshow(x_test[t], cmap = 'binary')
	t+=1
fig.tight_layout()    
plt.show()




# Look at the errors 
cols = 5
rows = math.ceil(len(errors[:15])/cols)
axes=[]
fig=plt.figure()
t=0
for i in errors[:15]:
	axes.append( fig.add_subplot(rows, cols, t+1) )
	subplot_title=("class "+str(y_pred[i])+"-->"+str(y_test[i]))
	axes[-1].set_title(subplot_title, fontsize=6)
	axes[-1].axes.set_axis_off()
	plt.imshow(x_test[i], cmap = 'binary')
	t+=1
fig.tight_layout()    
plt.show()




############################################## Heighth step

# Confusion matrix to see how the model performs
cm = confusion_matrix( y_test,y_pred,  labels=range(10))
df_cm = pd.DataFrame(cm, index = range(10),columns = range(10))
sn.heatmap(df_cm, annot=True, annot_kws={"size": 8},fmt='d') 
plt.show()




























    
    
    
    
